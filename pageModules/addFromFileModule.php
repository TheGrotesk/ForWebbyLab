<html>
	<body>
		<br>
		<form enctype="multipart/form-data" method="POST">
			
			<label>Выберете файл:</label>
			<input type="file" name="fileDb">
			<br>			
			<input type="submit" class="form-control btn btn-primary" value="Добавить">
		</form>
		<br>
			<?php
				if($_SERVER['REQUEST_METHOD'] == 'POST'){

						$array = array();
						$file = htmlspecialchars($_FILES['fileDb']['tmp_name']);
						$format_error = 0;
						$line = file($file);
							foreach ($line as $field=>$value) {
								$string = $value;
								$format_string = explode(':', $string,2);
								if(preg_match("/Title|Release Year|Format|Stars|^$/",$format_string['0'])){
											
										$erase_space = trim($format_string['1']);
										$array[$format_string['0']][]=$erase_space;	
										
								}else{
									echo '<lable style="color:red;">Неверный формат строки #'.$field.'('.$string.')</lable><br>';
									$format_error = 1;
								}
								
								
							}
							if($format_error != 1){
								
								for($i=0;$i<count($array['Title']);$i++){

									if(!preg_match_all("^[a-zA-Z0-9а-ЯА-яІіЇї]$^",$array['Title'][$i]) || !preg_match_all("^[a-zA-Z0-9а-ЯА-я]$^",$array['Release Year'][$i]) || !preg_match_all("^[a-zA-Z0-9а-ЯА-я]$^",$array['Format'][$i]) || !preg_match_all("^[a-zA-Z0-9а-ЯА-я]$^",$array['Stars'][$i])){

										echo $i.'. <lable style="color:red;">Фильм</lable> '.$array['Title'][$i].'<lable style="color:red;"> не добавлен, так как одно из полей (Title,Release Year,Format,Stars) не было заполнено!</lable><br>';
										continue;
									}else{
										
										$query = mysqli_query($main_conn,"INSERT INTO films(film_name,film_date,film_format,film_actors) VALUES ('".$array['Title'][$i]."','".$array['Release Year'][$i]."','".$array['Format'][$i]."','".$array['Stars'][$i]."') ");
										if($query){
										echo $i.'. <lable style="color:green;">Фильм</lable> '.$array['Title'][$i].'<lable style="color:green;"> добавлен успешно!</lable><br>';

										}else{
											echo mysqli_error($main_conn);
										}
									}
									
								}
								
							}else{
								echo '<label style="color:red;">Перед добавлением исправте Ваш файл!</label>';
							}
						
				}
			?>
	</body>
</html>
