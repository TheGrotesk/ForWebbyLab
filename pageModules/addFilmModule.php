<?php

$error = '';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if(isset($_POST)){
		$film_name = htmlspecialchars($_POST['film_name']);
		$film_date = htmlspecialchars($_POST['film_date']);
		$film_format = htmlspecialchars($_POST['film_format']);
		$film_actors = htmlspecialchars($_POST['film_actors']);

		if($film_name!="" && $film_date != "" && $film_format!="" && $film_actors != ""){
			$addFilm_query = mysqli_query($main_conn,"INSERT INTO films (film_name,film_date,film_format,film_actors) VALUES ('".$film_name."','".$film_date."','".$film_format."','".$film_actors."') ");
			if($addFilm_query){
				$error = '<label style="color:green;">Фильм </label> '.$film_name.' <label style="color:green;"> добавлен!</label>';
			}else{

				$error = '<label style="color:red;">Произошла ошибка!('.mysqli_error($main_conn).')</label>';
			}
		}else{
			$error = '<label style="color:red;">Заполните пустые поля!</label>';
		}
	}
}
?>
<html>
	<body>
		<br>
		<form method="POST">
			
			<label>Название фильма:</label>
			<input type="text" name="film_name" class="form-control" >
			<label>Год выпуска:</label>
			<select name="film_date" class="form-control" >
				<?php for($i=1920;$i<=date("Y");$i++): ?>
						<option><?php echo $i; ?></option>
				<?php endfor; ?>
			</select>
			<label>Формат:</label>
			<select name="film_format" class="form-control" >
				<option>DVD</option>
				<option>VHS</option>
				<option>Blue-Ray</option>
			</select>
			<label>Список актеров:</label>
			<input type="text" name="film_actors" class="form-control" >
			<br>
			<input type="submit" class="form-control btn btn-primary" value="Добавить">
			<?php echo $error; ?>
		</form>
	</body>
</html>