<html>
	<body>
		<br>

		<table class="table">
			<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Название 
						<?php if(!isset($_GET['search-key']) || !isset($_GET['search-key'])): ?>
						<a href="index.php?action=mainByName"><i class="fas fa-sort-alpha-up"></i></a> <a href="index.php?action=main"><i class="fas fa-sort-alpha-down"></i></a>
						<?php endif; ?>
					</th>
					<th scope="col">Действие</th>
				</tr>
				</thead>
					<tbody>

						<?php
							if(mysqli_num_rows($show_query) > 0){ 
							while($result = mysqli_fetch_assoc($show_query)):
						?>
						<tr>
					      <th scope="row"><?php echo $result['id']; ?></th>
					      <td><?php echo $result['film_name']; ?></td>
					      <td>
					      		<a href="index.php?action=info&id=<?php echo $result['id']; ?>" class="btn btn-primary"><i class="fas fa-info"></i></a>
					      		<a href="index.php?action=delete&id=<?php echo $result['id']; ?>" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
					      </td>
					    </tr>
					    <?php endwhile;}else{?>
					    	<td>Записей не найдено!</td>
					    <?php } ?>
					</tbody>
				</table>
	</body>
</html>