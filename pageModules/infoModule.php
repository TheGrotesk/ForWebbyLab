<html>
	<body>
		<br>
		<div class="card">
			<div class="card-body">
				<?php $result = mysqli_fetch_assoc($info_query); ?>
				<label><b>Название: </b> </label><?php echo $result['film_name']; ?><br>
				<label><b>Год выпуска: </b> </label><?php echo $result['film_date']; ?><br>
				<label><b>Формат: </b> </label><?php echo $result['film_format']; ?><br>
				<label><b>Актеры: </b> </label><?php echo $result['film_actors']; ?><br>
				<a href="index.php?action=delete&id=<?php echo $result['id']; ?>" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
			</div>
		</div>
	</body>
</html>