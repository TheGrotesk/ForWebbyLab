<?php
	
		ini_set('display_errors','1');
		include $_SERVER['DOCUMENT_ROOT'].'/config/db_Settings.php';
		$main_conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name) or launchSetup();
		mysqli_set_charset($main_conn,'utf8');

		function launchSetup(){
			echo '<label style="color:red;">Сервис не настроен. Перенаправление на страницу установки....</label>';
			header("Refresh:2; url=/config/install.php");
		}

		function showTable($main_conn,$sql){

			$show_query = mysqli_query($main_conn,$sql);
			if(!$show_query){
				echo mysqli_error($main_conn);
			}else{
				include $_SERVER['DOCUMENT_ROOT'].'/pageModules/tableModule.php';
			}
		}

		function addFilm($main_conn){

			include $_SERVER['DOCUMENT_ROOT'].'/pageModules/addFilmModule.php';

		}

		function addFilmFromFile($main_conn){

			include $_SERVER['DOCUMENT_ROOT'].'/pageModules/addFromFileModule.php';
		}

		function deleteFilm($main_conn,$sql){

			$delete_query = mysqli_query($main_conn,$sql);
			if(!$delete_query){
				echo mysqli_error($main_conn);
			}else{
				header("Location: index.php?action=".$_SESSION['action']);
			}
		}

		function showInfo($main_conn,$sql){
			$info_query = mysqli_query($main_conn,$sql);
			if(!$info_query){
				echo mysqli_error($main_conn);
			}else{
				include $_SERVER['DOCUMENT_ROOT'].'/pageModules/infoModule.php';
			}
		}


?>
