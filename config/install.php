<?php

	include $_SERVER['DOCUMENT_ROOT'].'/config/db_Settings.php';
	echo '<label style="color:green;">Установка начата</label><br>';

	createDb($db_host,$db_user,$db_pass,$db_name);

	function createDb($db_host,$db_user,$db_pass,$db_name){

				$install_conn = mysqli_connect($db_host,$db_user,$db_pass);
				$install_query = mysqli_query($install_conn,"CREATE DATABASE IF NOT EXISTS ".$db_name." CHARACTER SET utf8 COLLATE utf8_unicode_520_ci");
				mysqli_set_charset($install_conn,'utf8');
				if(mysqli_select_db($install_conn,$db_name)){
					if($install_query){
						echo '<label style="color:grey;">База данных '.$db_name.' создана успешно!</label><br>';
						createTable($install_conn,$db_host,$db_user,$db_pass,$db_name);
					}else{
						mysqli_close($install_conn);
						echo 'Error:'.mysqli_error($install_conn);
					}
				}
		}

	function createTable($install_conn,$db_host,$db_user,$db_pass,$db_name){

			$table_query = mysqli_query($install_conn,"CREATE TABLE films (id INT NOT NULL AUTO_INCREMENT,film_name VARCHAR(256) NOT NULL,film_date YEAR NULL,film_format VARCHAR(20) NOT NULL,film_actors TEXT NOT NULL, PRIMARY KEY(id)) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci");
			if(!$table_query){
				echo mysqli_error($install_conn);
				mysqli_close($install_conn);
			}else{
				mysqli_close($install_conn);
				echo '<label style="color:grey;">Таблица films создана успешно!</label><br>';
				echo '<label style="color:green;">Установка закончена!Перенаправялем на главную страницу....</label><br>';
				header("Refresh:4;url=http://".$_SERVER['HTTP_HOST']);
			}
	}
?>
