<!DOCTYPE html>

<html>

<head>
	<title>Film Searcher</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css">

	<!--BOOTSTRAP 4 AND FONT AWASOME -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>

	<div class="row w-100 h-100">
		<div class="col-md-3">
			<div class="card h-100 text-white bg-secondary">
				<div class="card-header ">
					<label><i class="fas fa-film"></i> Меню</label>
				</div>
				<div class="card-body">
					<div class="menu-item">
						<a href="index.php?action=main" class="btn btn-secondary"><i class="fas fa-scroll"></i> Главная</a>
					</div>
					<div class="menu-item">
						<a href="index.php?action=add" class="btn btn-secondary"><i class="fas fa-plus-square"></i> Добавить фильм</a>
					</div>
					<div class="menu-item">
						<a href="index.php?action=addFromFile" class="btn btn-secondary"><i class="fas fa-file-import"></i> Импорт из файла</a>
					</div>
					<br>
					<div class="menu-item" >
						<form method="GET">
							<input type="text" name="search-key" class="form-control" placeholder="Ключевое слово">
							<input type="submit" class="form-control btn btn-success" value="Найти">	
						</form>
						<br>
						<div class="card text-white bg-dark">
							<div class="card-body">
								<p>Поиск по букве,названию фильма,имени актера</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			<?php 
				include 'phpfiles/main.php';
				if(isset($_GET['search-key'])){
					$search_key = htmlspecialchars($_GET['search-key']);
					$sql = "SELECT * FROM films WHERE film_name LIKE '%".$search_key."%' OR film_actors LIKE '%".$search_key."%' ";
					showTable($main_conn,$sql);
				}else{

					if(!isset($_GET['action'])){
						header("Location: index.php?action=main");
					}else{
						$get = htmlspecialchars($_GET['action']);
						session_start();
						switch ($get) {
						 	case 'main':
						 		$_SESSION['action'] = $get;
						 		$sql = "SELECT * FROM films ORDER BY id";
								showTable($main_conn,$sql);
						 		break;
						 	case 'mainByName':
						 		$_SESSION['action'] = $get;
						 		$sql = "SELECT * FROM films ORDER BY film_name";
								showTable($main_conn,$sql);
								break;
							case 'add':
								addFilm($main_conn);
								break;
							case 'addFromFile':
								addFilmFromFile($main_conn);
								break;
							case 'delete':
								$id = htmlspecialchars($_GET['id']);
								$sql = "DELETE FROM films WHERE id='".$id."' LIMIT 1";
								deleteFilm($main_conn,$sql);
								break;
							case 'info':
								$id = htmlspecialchars($_GET['id']);
								$sql = "SELECT * FROM films WHERE id='".$id."' LIMIT 1";
								showInfo($main_conn,$sql);
						 	
						 } 
					}
				}
				
			?>
		</div>
	</div>
</body>
</html>
